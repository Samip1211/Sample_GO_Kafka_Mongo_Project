package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"../pkg/addToKafka"
	"../pkg/model"
)

var wg sync.WaitGroup

func recoverName() {
	if r := recover(); r != nil {
		log.Println("recovered from ", r)
		wg.Wait()
	}
}

func main() {
	//Take care of any panic
	defer recoverName()

	/*
		Function Handler
	*/
	http.HandleFunc("/v1/post", postToDB)

	http.HandleFunc("/v1/get", getFromDB)

	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		panic(err)
	}

	//Wait for all the go routines to finish
	wg.Wait()

}
func getFromDB(w http.ResponseWriter, r *http.Request) {

	var temp model.RequestData
	var respMsg model.Response

	if r.Method == "POST" {

		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&temp)

		if err != nil {
			fmt.Println(err)
		}

		msg, ok := validateGetRequest(temp)

		if ok {

			w.Header().Set("Content-Type", "application/json")

			respMsg.Code = 500

			resp, _ := json.Marshal(respMsg)

			w.Write(resp)

		} else {
			val := addToKafka.GetFromKafka(msg)

			//bs := []byte(strconv.Itoa(int(val)))
			if len(val) == 0 {
				//w.WriteHeader(http.StatusInternalServerError)

				respMsg.Code = 400
				respMsg.List = val

				resp, _ := json.Marshal(respMsg)
				w.Header().Set("Content-Type", "application/json")

				w.Write(resp)
			} else {

				respMsg.Code = 200
				respMsg.List = val

				resp, _ := json.Marshal(respMsg)

				w.Header().Set("Content-Type", "application/json")

				w.Write(resp)
			}

			//w.Write([]byte("Check"))
		}

	}

}

/*
	Validate and push to Kafka Queue, Queue will add the data in DB.
*/
func postToDB(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// Increment the go routine counter
		w.Header().Set("Content-Type", "application/json")

		decoder := json.NewDecoder(r.Body)

		var respMsg model.Response

		var temp model.RequestData

		err := decoder.Decode(&temp)

		if err != nil {
			respMsg.Code = 400

			resp, _ := json.Marshal(respMsg)

			w.Write(resp)
		}

		msg, ok := validatePostRequest(temp)

		if ok {
			respMsg.Code = 400

			resp, _ := json.Marshal(respMsg)

			w.Write(resp)
		} else {

			wg.Add(1)

			//Go routine to push to queue.
			go addToKafka.Push(&wg, msg)

			respMsg.Code = 202

			respMsg.List = []model.Data{msg.Person}

			resp, _ := json.Marshal(respMsg)

			w.Write(resp)
		}
	}

}
func validateGetRequest(val model.RequestData) (model.MessageData, bool) {

	var temp model.MessageData

	layout := "2006-01-02"

	t1, err := time.Parse(layout, val.Departure)

	if err != nil {
		fmt.Println(err)
		return temp, true
	}

	temp.Person = model.Data{Departure: t1, DepartureLoc: val.DepartureLoc, Weight: int64(val.Weight)}

	return temp, false

}
func validatePostRequest(val model.RequestData) (model.MessageData, bool) {

	var temp model.MessageData

	layout := "2006-01-02" //YYYY-MM-DD

	temp.Person = model.Data{Name: val.Name, DepartureLoc: val.DepartureLoc, ArrivalLoc: val.ArrivalLoc,

		Email: val.Email, Weight: int64(val.Weight)}

	t1, err1 := time.Parse(layout, val.Departure)
	t2, err2 := time.Parse(layout, val.Arrival)

	if err1 != nil || err2 != nil {
		fmt.Println(err1)
		fmt.Println(err2)
		return temp, true
	}

	phone, _ := strconv.Atoi(val.PhoneNum)
	temp.Person.PhoneNum = int64(phone)
	temp.Person.Departure = t1
	temp.Person.Arrival = t2

	return temp, false
}
