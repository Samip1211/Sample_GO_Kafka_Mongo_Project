package addToKafka

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"sync"
	"time"

	"../model"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func Push(w *sync.WaitGroup, tempData model.MessageData) {

	tempData.DbOp = 1 //Just Add the above data to DB.

	value, _ := json.Marshal(tempData)

	pushToQueue("myTopic", value) // Add the data in queue

	w.Done()

}

func GetFromKafka(msg model.MessageData) []model.Data {

	//tempData := setModelData("name4") //Set the variable to send

	//message.Person = tempData

	msg.DbOp = 0

	rand.Seed(time.Now().UnixNano())

	msg.ChanId = randomString(10) // Create a temporary topic to listen on.

	value, _ := json.Marshal(msg)

	pushToQueue("myTopic", value) // Add data in queue

	val := getFromQueue(msg.ChanId)

	return val
}

func pushToQueue(topic string, value []byte) {

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "kafka"})

	if err != nil {
		panic(err)
	}

	defer p.Close()

	// Delivery report handler for produced messages
	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					var msg bytes.Buffer
					msg.WriteString("Delivery failed ")
					//msg.WriteString(ev.TopicPartition)

					//fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
				} else {
					var msg bytes.Buffer
					msg.WriteString("Delivered message to ")
					//fmt.Println(reflect.TypeOf(ev.TopicPartition))
					msg.WriteString(*ev.TopicPartition.Topic)

					fmt.Printf("Delivered message to %v\n", ev.TopicPartition)
				}
			}
		}
	}()

	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          value,
	}, nil)

	p.Flush(15 * 1000)

}

func getFromQueue(ch string) []model.Data {

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "kafka",
		"group.id":          "myGroup",
		"auto.offset.reset": "earliest",
	})

	defer c.Close()

	if err != nil {
		panic(err)
	}

	c.SubscribeTopics([]string{ch}, nil)
	fmt.Println("Listening on new chan", ch)
	var val []model.Data

	fmt.Println("Listening")
	for {
		msg, err := c.ReadMessage(-1)
		if err == nil {
			e := json.Unmarshal(msg.Value, &val)
			if e != nil {

				fmt.Println("Error in unmarshalling")
				break
			}
			fmt.Printf("Value on queue %v", e)
			break
		} else {
			fmt.Printf("Consumer error: %v (%v)\n", err, msg)
			break
		}
	}

	return val

}
func randomInt(min, max int) int {
	return min + rand.Intn(max-min)
}

func randomString(len int) string {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		bytes[i] = byte(randomInt(65, 90))
	}
	return string(bytes)
}
