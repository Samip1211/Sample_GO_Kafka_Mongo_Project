package model

import "time"

type Data struct {
	Name         string    `json:"name" bson:"name"`
	DepartureLoc string    `json:"departureloc" bson:"departureloc"`
	ArrivalLoc   string    `json:"arrivalloc" bson:"arrivalloc"`
	Email        string    `json:"email" bson:"email"`
	Weight       int64     `json:"weight" bson:"weight"`
	Departure    time.Time `json:"departure" bson:"departure"`
	Arrival      time.Time `json:"arrival" bson:"arrival"`
	PhoneNum     int64     `json:"phonenum" bson:"phonenum"`
}

type MessageData struct {
	Person Data
	DbOp   int // 1 to create 0 to read
	ChanId string
}
