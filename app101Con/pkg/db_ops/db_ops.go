package db_ops

import (
	"fmt"
	"log"

	"../model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func recoverName() {
	if r := recover(); r != nil {
		log.Println("recovered from ", r)

	}
}

func InsertIntoDB(d model.Data) {

	session, err := mgo.Dial("mongo:27017")

	if err != nil {
		fmt.Println(err)
	}

	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("postMan")

	//data, _ := bson.Marshal(d)

	err = c.Insert(d)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Data is inserted")
	}

}

func SearchDB(val model.Data) []model.Data {

	defer recoverName()

	session, err := mgo.Dial("mongo:27017")

	if err != nil {
		fmt.Println(err)
	}

	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("postMan")

	result := []model.Data{}

	//rval := int(val.Weight)

	err = c.Find(bson.M{"departure": val.Departure}).All(&result)

	if err != nil {
		panic(err)

		return result
	}
	if len(result) == 0 {
		fmt.Println("Not found any")
		return result
	}

	fmt.Println("Weight:", result[0].Weight)
	fmt.Println("Name: ", result[0].Name)

	return result

}
