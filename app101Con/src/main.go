package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	"../pkg/db_ops"
	"../pkg/model"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

var wg sync.WaitGroup

func recoverName() {
	if r := recover(); r != nil {
		log.Println("recovered from ", r)

	}
}

func main() {

	defer recoverName()

	fmt.Println("Hello")
	time.Sleep(30 * time.Second)
	//Sleep 30s so as the kafka queue can be initialized
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "kafka",
		"group.id":          "myGroup",
		"auto.offset.reset": "earliest",
	})

	defer c.Close()

	if err != nil {
		panic(err)
	}

	wg.Add(1)
	defer wg.Done()

	go func() {
		c.SubscribeTopics([]string{"myTopic", "^aRegex.*[Tt]opic"}, nil)

		var temp2 model.MessageData
		//fmt.Println("Listening for data")
		for {
			msg, err := c.ReadMessage(-1)

			if err == nil {
				e := json.Unmarshal(msg.Value, &temp2)
				if e != nil {
					fmt.Println("Error in unmarshalling")
				}
				fmt.Printf("Message on %s: %s\n", msg.TopicPartition, temp2.Person.Name)

				if temp2.DbOp == 1 {
					go db_ops.InsertIntoDB(temp2.Person)

				} else {
					result := db_ops.SearchDB(temp2.Person)

					sendResult(temp2.ChanId, result)
				}

			} else {
				fmt.Printf("Consumer error: %v (%v)\n", err, msg)
				break
			}
		}

	}()
	//wg.Done()
	wg.Wait()
}

func sendResult(c string, a []model.Data) {

	defer recoverName()

	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "kafka"})

	if err != nil {
		panic(err)
	}
	defer p.Close()
	fmt.Println("Creating new chan", c)

	var wg1 sync.WaitGroup

	wg1.Add(1)
	go func() {
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					var msg bytes.Buffer
					msg.WriteString("Delivery failed ")
					//msg.WriteString(ev.TopicPartition)

					fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
					wg1.Done()
				} else {
					var msg bytes.Buffer
					msg.WriteString("Delivered message to ")
					//fmt.Println(reflect.TypeOf(ev.TopicPartition))
					msg.WriteString(*ev.TopicPartition.Topic)

					fmt.Printf("Delivered message to %v\n", ev.TopicPartition)
					wg1.Done()

				}
			}
		}
	}()

	topic := c

	value, _ := json.Marshal(a)
	fmt.Println(" sending data")
	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          value,
	}, nil)

	p.Flush(15 * 1000)
	wg1.Wait()
	fmt.Println("Done sending data")
}
