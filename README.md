## Introduction 
This is the sample application which fetches the information about person's travel details. The details are fetched based on the date he's travelling and amount of weight he would be willing to carry for other people. 


## Overview

Technologies Used: Go, Kafka, MongoDB, Docker.

## API's

Add the data in database:

URL: POST localhost:8080/v1/post
Data : 
```json
{
	"Name":"name2",
	"DepartureLoc":"AMD",
	"ArrivalLoc":"SJS",
	"Email":"kothari.samip@gmail.com",
	"Weight": 3,
	"PhoneNum": "6693509593",
	"Departure": "2017-06-12", //YYYY-MM-DD
	"Arrival": "2017-12-12"
	
}
```
Response:
```json
{
    "Code": 200,
    "List": [
        {
            "name": "name2",
            "departureloc": "AMD",
            "arrivalloc": "SJS",
            "email": "kothari.samip@gmail.com",
            "weight": 3,
            "departure": "2017-06-12T00:00:00Z",
            "arrival": "2017-12-12T00:00:00Z",
            "phonenum": 6693509593
        }
    ]
}
```
Get the data from database:

URL : POST localhost:8080/v1/get
Data :
```json
{
	
	"DepartureLoc":"AMD",
	"Weight": 3,
	"Departure": "2017-06-10"

}
```
Response:
```json
{
    "Code": 400,
    "List": [{
		"name": "name2",
        "departureloc": "AMD",
        "arrivalloc": "SJS",
        "email": "kothari.samip@gmail.com",
        "weight": 3,
        "departure": "2017-06-12T00:00:00Z",
        "arrival": "2017-12-12T00:00:00Z",
        "phonenum": 6693509593

	}]
}
```

### Running the Application.

Use the docker-compose file. Command: docker-compose up -d

